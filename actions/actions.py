import json
import os
from typing import Any

import requests
from dotenv import load_dotenv
from rasa_sdk import Tracker, FormValidationAction, Action
from rasa_sdk.events import SlotSet, FollowupAction
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk.types import DomainDict

from fbmessenger import BaseMessenger
from fbmessenger.thread_settings import GreetingText, MessengerProfile, GetStartedButton, PersistentMenu, \
    PersistentMenuItem

load_dotenv(dotenv_path=".env")
URL = os.environ['URL']
URL_RASA = os.environ['URL_RASA']
BASIC_TOKEN = os.environ['BASIC_TOKEN']
APP_SECRET = os.environ['APP_SECRET']
PAGE_ACCESS_TOKEN = os.environ['PAGE_ACCESS_TOKEN']

messenger = BaseMessenger(page_access_token=PAGE_ACCESS_TOKEN, app_secret=APP_SECRET)
greeting_text = GreetingText('Hi! how can I help you?')
get_started = GetStartedButton(payload='/get_started')
menu_item_1 = PersistentMenuItem(item_type='web_url', title='Visit out Facebook Page',
                                 url='https://www.facebook.com/BearBrandPH')
menu_item_2 = PersistentMenuItem(item_type='postback', title='Main Menu', payload='/main_menu')
menu = PersistentMenu(menu_items=[menu_item_1, menu_item_2])
messenger_profile = MessengerProfile(greetings=[greeting_text], get_started=get_started, persistent_menus=[menu])
messenger.set_messenger_profile(messenger_profile.to_dict())


class IsRegistered(Action):
    def name(self):
        return "IsRegistered"

    def run(self, dispatcher, tracker, domain):
        current_state = tracker.current_state()
        sender_id = current_state['sender_id']
        url = URL + "/api/bot/user/iscomplete"
        payload = json.dumps({
            "fbId": sender_id
        })
        headers = {
            'Authorization': 'Basic ' + BASIC_TOKEN,
            'Content-Type': 'application/json'
        }
        response = requests.request("POST", url, headers=headers, data=payload)
        print(response.text, response.status_code)
        response = response.json()
        if response["success"]:
            dispatcher.utter_message(template="utter_main_menu")
            return [SlotSet("phone_number", None), SlotSet("pin", None), SlotSet("phone_number_no", None),
                    SlotSet("otp", None),
                    SlotSet("otp_no", None), SlotSet("last_name", None), SlotSet("first_name", None),
                    SlotSet("last_name_no", None),
                    SlotSet("first_name_no", None), SlotSet("age", None), SlotSet("e_marketing_otp_in", None),
                    SlotSet("location", None)]
        else:
            dispatcher.utter_message(template="utter_welcome_screen")
            return [SlotSet("phone_number", None), SlotSet("pin", None), SlotSet("phone_number_no", None),
                    SlotSet("otp", None),
                    SlotSet("otp_no", None), SlotSet("last_name", None), SlotSet("first_name", None),
                    SlotSet("last_name_no", None),
                    SlotSet("first_name_no", None), SlotSet("age", None), SlotSet("e_marketing_otp_in", None),
                    SlotSet("location", None)]


class UnRegistered(Action):
    def name(self):
        return "UnRegistered"

    def run(self, dispatcher, tracker, domain):
        current_state = tracker.current_state()
        sender_id = current_state['sender_id']
        url = URL + "/api/bot/user/stop"
        payload = json.dumps({
            "fbId": sender_id
        })
        headers = {
            'Authorization': 'Basic ' + BASIC_TOKEN,
            'Content-Type': 'application/json'
        }
        requests.request("POST", url, headers=headers, data=payload)
        dispatcher.utter_message("You opted out to the PANALO SA TIBAY, PANALO SA BUHAY Raffle Promo.")
        return [SlotSet("phone_number", None), SlotSet("pin", None), SlotSet("phone_number_no", None),
                SlotSet("otp", None),
                SlotSet("otp_no", None), SlotSet("last_name", None), SlotSet("first_name", None),
                SlotSet("last_name_no", None),
                SlotSet("first_name_no", None), SlotSet("age", None), SlotSet("e_marketing_otp_in", None),
                SlotSet("location", None)]


class ValidatePhoneNumberForm(FormValidationAction):
    def name(self):
        return "validate_phone_number_form"

    def validate_phone_number(
            self,
            slot_value: Any,
            dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: DomainDict,
    ):
        return {"phone_number": slot_value}


class ValidatePhoneNumberNoForm(FormValidationAction):
    def name(self):
        return "validate_phone_number_no_form"

    def validate_phone_number_no(
            self,
            slot_value: Any,
            dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: DomainDict,
    ):
        return {"phone_number_no": slot_value}


class ValidateOtpForm(FormValidationAction):
    def name(self):
        return "validate_otp_form"

    def validate_otp(
            self,
            slot_value: Any,
            dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: DomainDict,
    ):
        return {"otp": slot_value}


class PhoneNumberSubmitOtp(Action):
    def name(self):
        return "action_submit_otp"

    def run(self, dispatcher, tracker, domain):

        """Validate `otp` value."""
        current_state = tracker.current_state()
        sender_id = current_state['sender_id']
        otp = tracker.get_slot("otp")
        url = URL + "/api/bot/otp/validate"
        payload = json.dumps({
            "fbId": sender_id,
            "otpcode": otp
        })
        headers = {
            'Authorization': 'Basic ' + BASIC_TOKEN,
            'Content-Type': 'application/json'
        }
        response = requests.request("POST", url, headers=headers, data=payload)
        print(response.text, response.status_code)
        data = response.json()
        if data["statusCode"] != 0:
            if data["attempts"] >= 3:
                return [FollowupAction(name="utter_attempt_more_3"), SlotSet("phone_number", None),
                        SlotSet("otp", None)]
            else:
                dispatcher.utter_message(template="utter_attempt_under_3")
                return [SlotSet("otp", None)]
        else:
            return [FollowupAction(name="GetFirstNameAction"), SlotSet("otp", None)]


class ValidateOtpNoForm(FormValidationAction):
    def name(self):
        return "validate_otp_no_form"

    def validate_otp_no(
            self,
            slot_value: Any,
            dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: DomainDict,
    ):
        return {"otp_no": slot_value}


class SubmitOtpNo(Action):
    def name(self):
        return "action_submit_otp_no"

    def run(self, dispatcher, tracker, domain):

        """Validate `otp` value."""
        current_state = tracker.current_state()
        sender_id = current_state['sender_id']
        otp = tracker.get_slot("otp_no")
        url = URL + "/api/bot/otp/validate"
        payload = json.dumps({
            "fbId": sender_id,
            "otpcode": otp
        })
        headers = {
            'Authorization': 'Basic ' + BASIC_TOKEN,
            'Content-Type': 'application/json'
        }
        response = requests.request("POST", url, headers=headers, data=payload)
        print(response.text, response.status_code)
        data = response.json()
        if data["statusCode"] != 0:
            if data["attempts"] >= 3:
                return [FollowupAction(name="utter_attempt_more_3_no"), SlotSet("phone_number_no", None),
                        SlotSet("otp_no", None)]
            else:
                dispatcher.utter_message(template="utter_attempt_under_3_no")
                return [SlotSet("otp_no", None)]
        else:
            return [FollowupAction(name="GetAgeAction"), SlotSet("otp_no", None)]


class PhoneNumberSubmitYes(Action):
    def name(self):
        return "action_submit_yes"

    def run(self, dispatcher, tracker, domain):

        """Validate `phone_number` value."""
        current_state = tracker.current_state()
        sender_id = current_state['sender_id']
        phone_number = tracker.get_slot("phone_number")
        url = URL_RASA + "/api/bot/reg/start"
        payload = json.dumps({
            "fbId": sender_id,
            "msisdn": phone_number
        })
        headers = {
            'Authorization': 'Basic ' + BASIC_TOKEN,
            'Content-Type': 'application/json'
        }
        response = requests.request("POST", url, headers=headers, data=payload)
        print(response.text, response.status_code)
        data = response.json()
        if data["format"]["isValid"]:
            if data["block"]["isBlocked"]:
                if data["block"]["type"] == 'validPin':
                    dispatcher.utter_message(template="utter_temporary_block")
                elif data["block"]["type"] == 'invalidPin':
                    dispatcher.utter_message(template="utter_permanent_block")
                dispatcher.utter_message(template="utter_welcome_screen")
                return [SlotSet("phone_number", None)]
            else:
                payload = json.dumps({
                    "fbId": sender_id,
                    "resetProp": "notRegistered"
                })
                url = URL + "/api/bot/reset/initval"
                requests.request("POST", url, headers=headers, data=payload)
                if data["reg"]["isRegistered"]:
                    return [FollowupAction(name="utter_get_otp"), SlotSet("phone_number", None)]
                else:
                    if data["reg"]["attempts"] >= 5:
                        dispatcher.utter_message(template="utter_attempt_more_5")
                        dispatcher.utter_message(template="utter_welcome_screen")
                        return [SlotSet("phone_number", None)]
                    else:
                        dispatcher.utter_message(template="utter_attempt_under_5_2")
                        return [SlotSet("phone_number", None)]
        else:
            if data["format"]["attempts"] >= 5:
                payload = json.dumps({
                    "fbId": sender_id,
                    "resetProp": "invalidFormat"
                })
                url = URL + "/api/bot/reset/initval"
                requests.request("POST", url, headers=headers, data=payload)
                dispatcher.utter_message(template="utter_attempt_more_5")
                dispatcher.utter_message(template="utter_welcome_screen")
                return [SlotSet("phone_number", None)]
            else:
                dispatcher.utter_message(template="utter_attempt_under_5")
                return [SlotSet("phone_number", None)]


class PhoneNumberSubmitNo(Action):
    def name(self):
        return "action_submit_no"

    def run(self, dispatcher, tracker, domain):

        """Validate `phone_number` value."""
        current_state = tracker.current_state()
        sender_id = current_state['sender_id']
        phone_number = tracker.get_slot("phone_number_no")
        url = URL_RASA + "/api/bot/reg/validate"
        payload = json.dumps({
            "fbId": sender_id,
            "msisdn": phone_number
        })
        headers = {
            'Authorization': 'Basic ' + BASIC_TOKEN,
            'Content-Type': 'application/json'
        }
        response = requests.request("POST", url, headers=headers, data=payload)
        print(response.text, response.status_code)
        data = response.json()
        if data["format"]["isValid"]:
            if data["block"]["isBlocked"]:
                if data["block"]["type"] == 'validPin':
                    dispatcher.utter_message(template="utter_temporary_block")
                elif data["block"]["type"] == 'invalidPin':
                    dispatcher.utter_message(template="utter_permanent_block")
                dispatcher.utter_message(template="utter_welcome_screen")
                return [SlotSet("phone_number_no", None)]
            else:
                if not data["isPreviouslyRegistered"]:
                    return [FollowupAction(name="utter_get_no_otp"), SlotSet("phone_number_no", None)]
                else:
                    dispatcher.utter_message(
                        'Sorry, but the mobile number you entered is already registered. You may now proceed to OTP verification.')
                    return [FollowupAction(name="utter_get_no_otp"), SlotSet("phone_number_no", None)]
        else:
            if data["format"]["attempts"] >= 5:
                payload = json.dumps({
                    "fbId": sender_id,
                    "resetProp": "invalidFormat"
                })
                url = URL + "/api/bot/reset/initval"
                requests.request("POST", url, headers=headers, data=payload)
                dispatcher.utter_message(template="utter_attempt_more_5")
                dispatcher.utter_message(template="utter_welcome_screen")
                return [SlotSet("phone_number_no", None)]
            else:
                dispatcher.utter_message(template="utter_attempt_under_no_5")
                return [SlotSet("phone_number_no", None)]


class GetFirstNameAction(Action):
    def name(self):
        return "GetFirstNameAction"

    def run(self, dispatcher, tracker, domain):
        return [SlotSet("first_name", None)]


class GetAgeAction(Action):
    def name(self):
        return "GetAgeAction"

    def run(self, dispatcher, tracker, domain):
        return []


class ValidateAgeForm(FormValidationAction):
    def name(self):
        return "validate_age_form"

    def validate_age(
            self,
            slot_value: Any,
            dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: DomainDict,
    ):
        return {"age": slot_value}


class AgeSubmit(Action):
    def name(self):
        return "action_submit_age"

    def run(self, dispatcher, tracker, domain):
        current_state = tracker.current_state()
        sender_id = current_state['sender_id']
        age = tracker.get_slot("age")
        url = URL + "/api/bot/reg/user"
        payload = json.dumps({
            "fbId": sender_id,
            "userUpdate": {
                "age": age
            }
        })
        headers = {
            'Authorization': 'Basic ' + BASIC_TOKEN,
            'Content-Type': 'application/json'
        }
        response = requests.request("POST", url, headers=headers, data=payload)
        print(response.text, response.status_code)
        data = response.json()
        if data["isAgeValid"]:
            return [FollowupAction(name="GetFirstNameNoAction")]
        else:
            dispatcher.utter_message("Sorry, ang iyong edad ay hindi po naaayon sa aming"
                                     "promo mechanics. Ang pwede lamang sumali sa raffle"
                                     "promo ay edad 18 pataas")
            dispatcher.utter_message(template="utter_welcome_screen")
        return [SlotSet("age", None)]


class ValidateFirstNameForm(FormValidationAction):
    def name(self):
        return "validate_first_name_form"

    def validate_first_name(
            self,
            slot_value: Any,
            dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: DomainDict,
    ):
        return {"first_name": slot_value}


class FirstNameSubmit(Action):
    def name(self):
        return "action_submit_first_name"

    def run(self, dispatcher, tracker, domain):
        first_name = tracker.get_slot("first_name")
        """Validate `otp` value."""
        current_state = tracker.current_state()
        sender_id = current_state['sender_id']
        url = URL + "/api/bot/reg/user"
        payload = json.dumps({
            "fbId": sender_id,
            "userUpdate": {
                "firstName": first_name
            }
        })
        headers = {
            'Authorization': 'Basic ' + BASIC_TOKEN,
            'Content-Type': 'application/json'
        }
        requests.request("POST", url, headers=headers, data=payload)
        text = '''
        Ikaw ba ay sigurado sa {} na nilagay mo? 
Kung hindi, click ang EDIT option par baguhin.
Kung sigurado ka na, click ang PROCEED option.
        '''.format(first_name)
        dispatcher.utter_message(text=text, buttons=[
            {"payload": "/edit_first_name", "title": "Edit"},
            {"payload": "/proceed_first_name", "title": "Proceed"},
        ])
        return [SlotSet("first_name", None)]


class GetFirstNameNoAction(Action):
    def name(self):
        return "GetFirstNameNoAction"

    def run(self, dispatcher, tracker, domain):
        return []


class ValidateFirstNameNoForm(FormValidationAction):
    def name(self):
        return "validate_first_name_no_form"

    def validate_first_name_no(
            self,
            slot_value: Any,
            dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: DomainDict,
    ):
        return {"first_name_no": slot_value}


class FirstNameNoSubmit(Action):
    def name(self):
        return "action_submit_first_name_no"

    def run(self, dispatcher, tracker, domain):
        first_name = tracker.get_slot("first_name_no")
        """Validate `otp` value."""
        current_state = tracker.current_state()
        sender_id = current_state['sender_id']
        url = URL + "/api/bot/reg/user"
        payload = json.dumps({
            "fbId": sender_id,
            "userUpdate": {
                "firstName": first_name
            }
        })
        headers = {
            'Authorization': 'Basic ' + BASIC_TOKEN,
            'Content-Type': 'application/json'
        }
        requests.request("POST", url, headers=headers, data=payload)
        text = '''
        Ikaw ba ay sigurado sa {} na nilagay mo? 
Kung hindi, click ang EDIT option par baguhin.
Kung sigurado ka na, click ang PROCEED option.
        '''.format(first_name)
        dispatcher.utter_message(text=text, buttons=[
            {"payload": "/edit_first_name_no", "title": "Edit"},
            {"payload": "/proceed_first_name_no", "title": "Proceed"},
        ])
        return [SlotSet("first_name_no", None)]


class ValidateLastNameForm(FormValidationAction):
    def name(self):
        return "validate_last_name_form"

    def validate_last_name(
            self,
            slot_value: Any,
            dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: DomainDict,
    ):
        return {"last_name": slot_value}


class LastNameSubmit(Action):
    def name(self):
        return "action_submit_last_name"

    def run(self, dispatcher, tracker, domain):
        last_name = tracker.get_slot("last_name")
        """Validate `otp` value."""
        current_state = tracker.current_state()
        sender_id = current_state['sender_id']
        url = URL + "/api/bot/reg/user"
        payload = json.dumps({
            "fbId": sender_id,
            "userUpdate": {
                "lastName": last_name
            }
        })
        headers = {
            'Authorization': 'Basic ' + BASIC_TOKEN,
            'Content-Type': 'application/json'
        }
        response = requests.request("POST", url, headers=headers, data=payload)
        print(response.text, response.status_code)
        data = response.json()
        text = '''
        Ikaw ba ay sigurado sa {} na nilagay mo? 
Kung hindi, click ang EDIT option par baguhin.
Kung sigurado ka na, click ang PROCEED option.
        '''.format(last_name)
        dispatcher.utter_message(text=text, buttons=[
            {"payload": "/edit_last_name", "title": "Edit"},
            {"payload": "/proceed_last_name", "title": "Proceed"},
        ])
        return [SlotSet("e_marketing_otp_in", data["marketingOptIn"]), SlotSet("last_name", None)]


class ValidateLastNameNoForm(FormValidationAction):
    def name(self):
        return "validate_last_name_no_form"

    def validate_last_name_no(
            self,
            slot_value: Any,
            dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: DomainDict,
    ):
        return {"last_name_no": slot_value}


class LastNameNoSubmit(Action):
    def name(self):
        return "action_submit_last_name_no"

    def run(self, dispatcher, tracker, domain):
        last_name = tracker.get_slot("last_name_no")
        """Validate `otp` value."""
        current_state = tracker.current_state()
        sender_id = current_state['sender_id']
        url = URL + "/api/bot/reg/user"
        payload = json.dumps({
            "fbId": sender_id,
            "userUpdate": {
                "lastName": last_name
            }
        })
        headers = {
            'Authorization': 'Basic ' + BASIC_TOKEN,
            'Content-Type': 'application/json'
        }
        requests.request("POST", url, headers=headers, data=payload)
        text = '''
        Ikaw ba ay sigurado sa {} na nilagay mo? 
Kung hindi, click ang EDIT option par baguhin.
Kung sigurado ka na, click ang PROCEED option.
        '''.format(last_name)
        dispatcher.utter_message(text=text, buttons=[
            {"payload": "/edit_last_name_no", "title": "Edit"},
            {"payload": "/proceed_last_name_no", "title": "Proceed"},
        ])
        return [SlotSet("last_name_no", None)]


class AddLocation(Action):
    def name(self):
        return "AddLocation"

    def run(self, dispatcher, tracker, domain):
        location = tracker.get_slot("location")
        """Validate `otp` value."""
        current_state = tracker.current_state()
        sender_id = current_state['sender_id']
        url = URL + "/api/bot/reg/user"
        payload = json.dumps({
            "fbId": sender_id,
            "userUpdate": {
                "location": location
            }
        })
        headers = {
            'Authorization': 'Basic ' + BASIC_TOKEN,
            'Content-Type': 'application/json'
        }
        requests.request("POST", url, headers=headers, data=payload)
        dispatcher.utter_message(template="utter_congrats")
        dispatcher.utter_message(template="utter_receive_promotional_messages")
        return []


class REMARKETINGOPTINSubmitYes(Action):
    def name(self):
        return "REMARKETINGOPTINSubmitYes"

    def run(self, dispatcher, tracker, domain):
        """Validate `otp` value."""
        current_state = tracker.current_state()
        sender_id = current_state['sender_id']
        url = URL + "/api/bot/reg/user"
        payload = json.dumps({
            "fbId": sender_id,
            "userUpdate": {
                "marketingOptIn": "yes"
            }
        })
        headers = {
            'Authorization': 'Basic ' + BASIC_TOKEN,
            'Content-Type': 'application/json'
        }
        requests.request("POST", url, headers=headers, data=payload)
        dispatcher.utter_message(template="utter_receive_promotional_messages_yes")
        dispatcher.utter_message(template="utter_finale_1")
        dispatcher.utter_message(template="utter_finale_2")
        dispatcher.utter_message(template="utter_main_menu")
        return []


class REMARKETINGOPTINSubmitNo(Action):
    def name(self):
        return "REMARKETINGOPTINSubmitNo"

    def run(self, dispatcher, tracker, domain):
        """Validate `otp` value."""
        current_state = tracker.current_state()
        sender_id = current_state['sender_id']
        url = URL + "/api/bot/reg/user"
        payload = json.dumps({
            "fbId": sender_id,
            "userUpdate": {
                "marketingOptIn": "no"
            }
        })
        headers = {
            'Authorization': 'Basic ' + BASIC_TOKEN,
            'Content-Type': 'application/json'
        }
        requests.request("POST", url, headers=headers, data=payload)
        dispatcher.utter_message(template="utter_receive_promotional_messages_no")
        dispatcher.utter_message(template="utter_finale_1")
        dispatcher.utter_message(template="utter_finale_2")
        dispatcher.utter_message(template="utter_main_menu")
        return []


class VerifyREMARKETINGOPTIN(Action):
    def name(self):
        return "VerifyR_EMARKETING_OPT_IN"

    def run(self, dispatcher, tracker, domain):
        e_marketing_otp_in = tracker.get_slot("e_marketing_otp_in")
        if e_marketing_otp_in == "noAnswer":
            dispatcher.utter_message(template="utter_receive_promotional_messages")
            return []
        else:
            dispatcher.utter_message(template="utter_finale_1")
            dispatcher.utter_message(template="utter_finale_2")
            dispatcher.utter_message(template="utter_main_menu")
            return []


class SubukanUlitYesAction(Action):
    def name(self):
        return "SubukanUlitYesAction"

    def run(self, dispatcher, tracker, domain):
        current_state = tracker.current_state()
        sender_id = current_state['sender_id']
        url = URL + "/api/bot/reset/initval"
        payload = json.dumps({
            "fbId": sender_id,
            "resetProp": "invalidFormat"
        })
        headers = {
            'Authorization': 'Basic ' + BASIC_TOKEN,
            'Content-Type': 'application/json'
        }
        requests.request("POST", url, headers=headers, data=payload)
        return []


class SubukanUlitNoAction(Action):
    def name(self):
        return "SubukanUlitNoAction"

    def run(self, dispatcher, tracker, domain):
        current_state = tracker.current_state()
        sender_id = current_state['sender_id']
        url = URL + "/api/bot/reset/initval"
        payload = json.dumps({
            "fbId": sender_id,
            "resetProp": "invalidFormat"
        })
        headers = {
            'Authorization': 'Basic ' + BASIC_TOKEN,
            'Content-Type': 'application/json'
        }
        requests.request("POST", url, headers=headers, data=payload)
        return []


class EntriesCountAction(Action):
    def name(self):
        return "EntriesCountAction"

    def run(self, dispatcher, tracker, domain):
        current_state = tracker.current_state()
        sender_id = current_state['sender_id']
        url = URL + "/api/bot/entries/count"
        payload = json.dumps({
            "fbId": sender_id
        })
        headers = {
            'Authorization': 'Basic ' + BASIC_TOKEN,
            'Content-Type': 'application/json'
        }
        data = requests.request("POST", url, headers=headers, data=payload).json()
        count = 0
        if "count" in data.keys():
            count = data["count"]
        text = '''
        You have a total of {} number of Raffle Entries. 
Bumili lang ng BEAR BRAND POWDERED MILK DRINK
products for more chances of winning! 
Maraming salamat at good luck!
                '''.format(count)
        dispatcher.utter_message(text=text, buttons=[
            {"payload": "/main_menu", "title": "Main Menu"}
        ])
        return []


class PromoMechanicsAction(Action):
    def name(self):
        return "PromoMechanicsAction"

    def run(self, dispatcher, tracker, domain):
        current_state = tracker.current_state()
        sender_id = current_state['sender_id']
        url = URL + "/api/bot/user/iscomplete"
        payload = json.dumps({
            "fbId": sender_id
        })
        headers = {
            'Authorization': 'Basic ' + BASIC_TOKEN,
            'Content-Type': 'application/json'
        }
        response = requests.request("POST", url, headers=headers, data=payload)
        print(response.text, response.status_code)
        response = response.json()
        if response["success"]:
            dispatcher.utter_message(template="utter_promo_mechanics_1")
            return []
        else:
            dispatcher.utter_message(template="utter_promo_mechanics_2")
            return []


class IbaPangConcernAction(Action):
    def name(self):
        return "IbaPangConcernAction"

    def run(self, dispatcher, tracker, domain):
        current_state = tracker.current_state()
        sender_id = current_state['sender_id']
        url = URL + "/api/bot/user/iscomplete"
        payload = json.dumps({
            "fbId": sender_id
        })
        headers = {
            'Authorization': 'Basic ' + BASIC_TOKEN,
            'Content-Type': 'application/json'
        }
        response = requests.request("POST", url, headers=headers, data=payload)
        print(response.text, response.status_code)
        response = response.json()
        if response["success"]:
            dispatcher.utter_message(template="utter_Iba_Pang_Concern_1")
            return []
        else:
            dispatcher.utter_message(template="utter_Iba_Pang_Concern_2")
            return []


class SubmitRaffleEntryAction(Action):
    def name(self):
        return "SubmitRaffleEntryAction"

    def run(self, dispatcher, tracker, domain):
        current_state = tracker.current_state()
        sender_id = current_state['sender_id']
        url = URL + "/api/bot/user/fraud"
        payload = json.dumps({
            "fbId": sender_id
        })
        headers = {
            'Authorization': 'Basic ' + BASIC_TOKEN,
            'Content-Type': 'application/json'
        }
        response = requests.request("POST", url, headers=headers, data=payload)
        print(response.text, response.status_code)
        response = response.json()
        if "temporary" not in response:
            return [FollowupAction(name="IsRegistered")]
        elif response["temporary"]:
            text = '''Your number is temporary blocked due to maximum failed
attempts in a day. Please try again after 24 hours. 
Para sa iba pang katanungan o concern, maaari kayong
tumawag sa aming Consumer Care Hotline at (+632)
8898-0061 o mag-email sa
consumer.services@ph.nestle.com.'''
            dispatcher.utter_message(text=text)
            dispatcher.utter_message(template="utter_main_menu")
            return []
        else:
            return [FollowupAction(name="GetPinAction")]


class GetPinAction(Action):
    def name(self):
        return "GetPinAction"

    def run(self, dispatcher, tracker, domain):
        return []


class ValidatePinForm(FormValidationAction):
    def name(self):
        return "validate_pin_form"

    def validate_pin(
            self,
            slot_value: Any,
            dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: DomainDict,
    ):
        return {"pin": slot_value}


class SubmitPinAction(Action):
    def name(self):
        return "SubmitPinAction"

    def run(self, dispatcher, tracker, domain):

        """Validate `phone_number` value."""
        current_state = tracker.current_state()
        sender_id = current_state['sender_id']
        pin = tracker.get_slot("pin")
        url = URL + "/api/bot/submit/pin"
        payload = json.dumps({
            "fbId": sender_id,
            "pin": pin
        })
        headers = {
            'Authorization': 'Basic ' + BASIC_TOKEN,
            'Content-Type': 'application/json'
        }
        response = requests.request("POST", url, headers=headers, data=payload)
        print(response.text, response.status_code)
        data = response.json()
        if "isPinValid" not in data.keys():
            return [FollowupAction(name="IsRegistered")]
        elif data["isPinValid"]:
            if data["success"]:
                payload = json.dumps({
                    "fbId": sender_id
                })
                url = URL + "/api/bot/entries/count"
                data = requests.request("POST", url, headers=headers, data=payload).json()
                count = 0
                if "count" in data.keys():
                    count = data["count"]
                text = '''Congratulations! You have successfully submitted your
Raffle Entry. You have a total of {} number of Raffle Entry.
Bumili lang ng BEAR BRAND FORTIFIED POWDERED
MILK DRINK products for more chances of winning!
To submit more entries, click SUBMIT RAFFLE ENTRY.'''.format(count)
                dispatcher.utter_message(text=text, buttons=[
                    {"payload": "/Submit_Raffle_Entry", "title": "Submit Raffle Entry"},
                    {"payload": "/main_menu", "title": "Main Menu"}
                ])
                return [SlotSet("pin", None)]
            elif data["isPreviouslyRedeemed"]:
                dispatcher.utter_message(template="utter_fb_quick_reply_example")
                return [SlotSet("pin", None)]
            elif data["block"]["isBlocked"] and data["block"]["type"] == "validPin":
                text = '''You've reached the maximum 
                        number of registration entries in a minute.
                        Your number is permanently blocked to join this promo. For
                        issues, please chat with Globe at m.me/globeph or at TM
                        at m.me/TMtambayan'''
                dispatcher.utter_message(text=text)
                dispatcher.utter_message(template="utter_welcome_screen")
                return [SlotSet("pin", None)]
        else:
            if data["block"]["isBlocked"] and data["block"]["type"] == "invalidPin":
                text = '''Sorry, your number is permanently blocked from joining
                this promo due to maximum number of failed attempts in a
                minute.
                Para sa iba pang katanungan o concern, maaari kayong
                tumawag sa aming Consumer Care Hotline at (+632)
                8898-0061 o mag-email sa
                consumer.services@ph.nestle.com..'''
                dispatcher.utter_message(text=text)
                dispatcher.utter_message(template="utter_welcome_screen")
                return [SlotSet("pin", None)]
            elif data["block"]["isBlocked"] and data["block"]["type"] == "temporary":
                text = '''Sorry, you've reached the maximum number of redemption
                attempts. Please try again after 24 hours. 
                Para sa iba pang katanungan o concern, maaari kayong
                tumawag sa aming Consumer Care Hotline at (+632)
                8898-0061 o mag-email sa
                consumer.services@ph.nestle.com.'''
                dispatcher.utter_message(text=text)
                dispatcher.utter_message(template="utter_main_menu")
                return [SlotSet("pin", None)]
            elif not data["success"]:
                dispatcher.utter_message(template="utter_fb_quick_reply_example_2")
                return [SlotSet("pin", None)]
